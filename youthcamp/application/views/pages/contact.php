<div class='custom'>
	<div class='row'>
		<div class='well'>
			<h2>Contact Persons</h2>
			<br />
			<table class='table table-bordered table-striped'>
		    <thead>
			    <tr>
			    	<th>Role</th>
				    <th>Contact Person </th>
				    <th>Phone Number </th>
			    </tr>
		    </thead>
		    <tbody>
		    	<tr>
					<td>Head</td>
					<td>Babatunde Ampah</td>
					<td>0264055555</td>
				</tr>
			   <tr>
			   		<td>Registration</td>
					<td>Andrew Amegatcher</td>
					<td>0267666975</td>
				</tr>
				<tr>
					<td>Registration</td>
					<td>Anthony Dzixose-Davor</td>
					<td>0244226692</td>
				</tr>
				
				<tr>
					<td>Transport</td>
					<td>Sipho Twum</td>
					<td>0244233994</td>
				</tr>
				<tr>
					<td>Payments</td>
					<td>Kwamena Odum </td>
					<td>0244835721</td>
				</tr>
		    </tbody>
	    </table>
		</div>
	    
	</div>
</div>
