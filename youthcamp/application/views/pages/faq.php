<div class='custom'>
	<div class="row ">
		<div class='well faq'>
			<h2>FAQ's</h2>
			<br />
			<ol>
				<li>
					<div class='question'>How much will it cost to attend camp this year?</div>
					<div class='answer'>This year’s camp will cost each camper a highly subsidised fee of GHC70.</div>
				</li>
				<li>
					<div class='question'>When is the deadline for registration?</div>
					<div class='answer'>Deadline for registration is 10th June 2012. 
						Registration is only complete upon payment and failure to make payment 
						before the registration deadline will result in a forfeiture of registrant’s place at camp. 
						To avoid this situation, campers are required to inform registration officers beforehand of 
						their inability to meet the deadline.
					</div>
				</li>
				<li>
					<div class='question'>Where can I make payment?</div>
					<div class='answer'> Payments can be made at the Ridge and Manet Church Offices from Monday 
						to Friday and on Sundays at registration desk.
					</div>
				</li>
				<!--<li>
					<div class='question'>Are there any discounts for families with multiple members attending camp?</div>
					<div class='answer'>  
						Yes, families with 3 or more members attending camp will receive a 15% discount 
						on the total amount they would have had to pay.
					</div>
				</li>-->
				<!-- <li>
					<div class='question'>What do I do if I want to attend camp but can’t afford it?</div>
					<div class='answer'>There is currently no provision for sponsorship. However campers who</div>
				</li> -->
				<!--<li>
					<div class='question'>Will I still have to pay GHC70 if I don’t make it on Wednesday?</div>
					<div class='answer'> Campers who will be at camp anytime between Thursday (14th June ) and Saturday (16th June) 
						will be required to pay a camp fee of GHC60.
					</div>
				</li>-->
				<li>
					<div class='question'>Can I receive a refund after having made payment?</div>
					<div class='answer'>Camp fees once paid are not refundable.</div>
				</li>
				<!--<li>
					<div class='question'>Are there transportation arrangements for campers who will be coming to camp at the weekend?</div>
					<div class='answer'>There are transportation arrangements for campers who will be coming to camp on Friday evening however 
						anyone who intends to take advantage of this arrangement is required to register their 
						interest with <b>Sipho Twum on 0244233994  </b> .
					</div>
				</li>-->
				<li>
					<div class='question'>Can I attend camp even if I haven’t made payment?</div>
					<div class='answer'> Campers who for one reason or other cannot make payments before the deadline are required to inform the 
						the camp officers 	latest by the close of day on the 10th of June 2012. They will however be required to make payments latest by 1st July 2012.
						 <br />
						 Registrants who fail to make payment arrangements with the listed camp officers will be considered to have redrawn their application.
					</div>
				</li>
				<!-- <li>
					<div class='question'></div>
					<div class='answer'></div>
				</li> -->
			</ol>	
		</div>		
	</div>
</div>